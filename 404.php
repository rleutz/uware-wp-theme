<?php get_header(); ?>


<style>

#footer {
    position: absolute;
    bottom: 0;
    width:98%;
}

</style>

<div class="mt-5">
  <div class="row text-center">
      <div class="col-md-3"></div>
			<div class="col-md-3">
				<h1 class="text-center display-1 mt-3"><span class="badge bg-danger text-white">404</span></h1>
			</div>
      <div class="col-md-3">

				<h1 class="text-secondary">A página não existe.</h1>

				<p class="lead">Tente procurar pela página.</p>
				<?php get_search_form(); ?>

      </div>
      <div class="col-md-3"></div>
  </div>
</div>



<?php get_footer(); ?>
