<?php

function uw_enqueue(){
    $uri = get_theme_file_uri();
    $ver = UW_DEV_MODE ? time() : false;


    // Styles
    wp_register_style(
        'uw_bootstrap',
        $uri . '/assets/bootstrap/css/bootstrap.css',
        [],
        $ver
    );
    wp_register_style(
        'uw_styles',
        $uri . '/assets/css/styles.css',
        [],
        $ver
    );
    wp_register_style(
        'uw_normalize',
        $uri . '/assets/css/normalize.css',
        [],
        $ver
    );

    wp_enqueue_style( 'uw_bootstrap' );
    // wp_enqueue_style( 'uw_normalize' );
    wp_enqueue_style( 'uw_styles' );


    // Scripts
    wp_register_script( 'uw_script-async', $uri . '/assets/js/script.js', [], $ver, true );
    wp_register_script( 'uw_awesome-async', $uri . '/assets/js/awesome.js', [], $ver, true );
    wp_register_script( 'uw_bootstrap-async', $uri . '/assets/bootstrap/js/bootstrap.bundle.js', [], $ver, true);

    wp_enqueue_script( 'uw_script-async' );
    wp_enqueue_script( 'uw_awesome-async' );
    wp_enqueue_script( 'uw_bootstrap-async' );
}
