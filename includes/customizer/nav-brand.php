<?php

function uw_nav_brand_customizer_section( $wp_customize ){

  // Section
  $wp_customize->add_section( 'uw_nav_brand_section', [
    'title'         =>  __( 'Nav Brand', 'uware' ),
    'priority'      =>  40,
    'panel'         =>  'uware'
  ]);

  // Setting
  $wp_customize->add_setting( 'uw_nav_brand_handle',[
    'capability'        => 'edit_theme_options',
    'default'           => '',
    'sanitize_callback' => 'ic_sanitize_image'
  ]);
  $wp_customize->add_setting( 'uw_nav_brand_left', [
    'default'           =>  '10px'
    ] );

  // Control
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_nav_brand_handle',
    [
      'label'       =>  __('Brand Image', 'uware'),
      'settings'    =>  'uw_nav_brand_handle',
      'section'     =>  'uw_nav_brand_section',
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_nav_brand_left',
    [
      'label'       =>  __('Left Side Space', 'uware'),
      'settings'    =>  'uw_nav_brand_left',
      'section'     =>  'uw_nav_brand_section',
      'type'        =>  'text'
    ]
  ));
}
function ic_sanitize_image( $file, $setting ) {

    $mimes = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif'          => 'image/gif',
        'png'          => 'image/png',
        'bmp'          => 'image/bmp',
        'tif|tiff'     => 'image/tiff',
        'ico'          => 'image/x-icon',
        'WebP'         => 'image/webp'
    );

    //check file type from file name
    $file_ext = wp_check_filetype( $file, $mimes );

    //if file has a valid mime type return it, otherwise return default
    return ( $file_ext['ext'] ? $file : $setting->default );
}
