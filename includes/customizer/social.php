<?php

function uw_social_customizer( $wp_customize ){

  // Section
  $wp_customize->add_section( 'uw_social_section', [
      'title'     =>  __( 'Soccial Settings', 'uware' ),
      'priority'  =>  20,
      'panel'     =>  'uware'
  ]);

  // Settings
  $wp_customize->add_setting( 'uw_social_title_handle', [
      'default'   =>  __( 'Social Media', 'uware' )
  ] );

  $wp_customize->add_setting( 'uw_facebook_handle', [
      'default'   =>  ''
  ] );

  $wp_customize->add_setting( 'uw_twitter_handle', [
      'default'   =>  ''
  ] );

  $wp_customize->add_setting( 'uw_youtube_handle', [
      'default'   =>  ''
  ] );

  $wp_customize->add_setting( 'uw_telegram_handle', [
      'default'   =>  ''
  ] );
  $wp_customize->add_setting( 'uw_social_side_present', [
    'default'       =>  'yes',
  ]);

  // Control
  $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      'uw_social_title_input',
      array(
          'label'          => __( 'Social Title', 'uware' ),
          'section'        => 'uw_social_section',
          'settings'       => 'uw_social_title_handle'
      )
  ) );

  $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      'uw_social_facebook_input',
      array(
          'label'          => __( 'Facebook Handle', 'uware' ),
          'section'        => 'uw_social_section',
          'settings'       => 'uw_facebook_handle'
      )
  ) );

  $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      'uw_social_twitter_input',
      array(
          'label'          => __( 'Twitter Handle', 'uware' ),
          'section'        => 'uw_social_section',
          'settings'       => 'uw_twitter_handle'
      )
  ) );

  $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      'uw_social_youtube_input',
      array(
          'label'          => __( 'Youtube Handle', 'uware' ),
          'section'        => 'uw_social_section',
          'settings'       => 'uw_youtube_handle'
      )
  ) );

  $wp_customize->add_control( new WP_Customize_Control(
      $wp_customize,
      'uw_social_telegram_input',
      array(
          'label'          => __( 'Telegram Handle', 'uware' ),
          'section'        => 'uw_social_section',
          'settings'       => 'uw_telegram_handle'
      )
  ) );
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'uw_social_side_present',[
      'label'       =>  __( 'Show Social Side Menu', 'uware' ),
      'section'     =>  'uw_social_section',
      'settings'    =>  'uw_social_side_present',
      'type'        =>  'checkbox',
      'choices'     =>  [
        'yes'       =>  'Yes'
      ]
    ]
  ));
}
