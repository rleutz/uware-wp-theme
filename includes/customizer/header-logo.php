<?php

function uw_header_customizer_section( $wp_customize ){

  // Section
  $wp_customize->add_section( 'uw_header_section', [
    'title'         =>  __( 'Header Logo', 'uware' ),
    'priority'      =>  30,
    'panel'         =>  'uware'
  ]);

  // Settings
  $wp_customize->add_setting( 'uw_header_present', [
    'default'       =>  'yes',
  ]);

  // Controller
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'uw_header_present',[
      'label'       =>  __( 'Show Header and Logo', 'uware' ),
      'section'     =>  'uw_header_section',
      'settings'    =>  'uw_header_present',
      'type'        =>  'checkbox',
      'choices'     =>  [
        'yes'       =>  'Yes'
      ]
    ]
  ));


}
