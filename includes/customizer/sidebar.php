<?php

function uw_sidebar_customize_section( $wp_customize ){
  // Section
  $wp_customize->add_section( 'uw_sidebar_section', [
      'title'     =>  __( 'Sidebar Settings', 'uware' ),
      'priority'  =>  50,
      'panel'     =>  'uware'
  ]);

  // Settings
  $wp_customize->add_setting( 'uw_sidebar_right_toogle', [
    'default'       =>  'yes',
  ]);
  $wp_customize->add_setting( 'uw_sidebar_full_width_toogle', [
    'default'       =>  'yes',
  ]);
  $wp_customize->add_setting( 'uw_post_cols_number', [
    'default'       =>  '8',
  ]);

  // Control
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'uw_post_cols_toogle',[
      'label'       =>  __( 'Post width cols', 'uware' ),
      'section'     =>  'uw_sidebar_section',
      'settings'    =>  'uw_post_cols_number',
      'type'        =>  'select',
      'choices'     =>  [
        '1'         =>  __( '1', 'uware' ),
        '2'         =>  __( '2', 'uware' ),
        '3'         =>  __( '3', 'uware' ),
        '4'         =>  __( '4', 'uware' ),
        '5'         =>  __( '5', 'uware' ),
        '6'         =>  __( '6', 'uware' ),
        '7'         =>  __( '7', 'uware' ),
        '8'         =>  __( '8', 'uware' ),
        '9'         =>  __( '9', 'uware' ),
        '10'         =>  __( '10', 'uware' ),
        '11'         =>  __( '11', 'uware' ),
        '12'         =>  __( '12', 'uware' )
      ]
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'uw_sidebar_right_toogle',[
      'label'       =>  __( 'Show sidebar right', 'uware' ),
      'description' =>  __( 'Only works without full width', 'uware' ),
      'section'     =>  'uw_sidebar_section',
      'settings'    =>  'uw_sidebar_right_toogle',
      'type'        =>  'checkbox',
      'choices'     =>  [
        'yes'       =>  'Yes'
      ]
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'uw_sidebar_full_width_toogle',[
      'label'       =>  __( 'Show full width', 'uware' ),
      'section'     =>  'uw_sidebar_section',
      'settings'    =>  'uw_sidebar_full_width_toogle',
      'type'        =>  'checkbox',
      'choices'     =>  [
        'yes'       =>  'Yes'
      ]
    ]
  ));
}
