<?php

function uw_footer_customizer_section( $wp_customize ){

  // Section
  $wp_customize->add_section( 'uw_footer_section', [
    'title'         =>  __( 'Footer', 'uware' ),
    'priority'      =>  150,
    'panel'         =>  'uware'
  ]);

  // Settings
  $wp_customize->add_setting( 'uw_footer_copyright_text', [
    'default'       =>  'Copyrights &copy; 2021 <a href="https://uware.com.br">uware.com.br</a>'
  ]);
  $wp_customize->add_setting( 'uw_footer_left_image_handle',[
    'capability'        => 'edit_theme_options',
    'default'           => '',
    'sanitize_callback' => 'ic_sanitize_image'
  ]);
  $wp_customize->add_setting( 'uw_footer_left_image_link',[
    'default'           =>  ''
  ]);
  $wp_customize->add_setting( 'uw_footer_right_image_link',[
    'default'           =>  ''
  ]);
  $wp_customize->add_setting( 'uw_footer_right_image_handle',[
    'capability'        => 'edit_theme_options',
    'default'           => '',
    'sanitize_callback' => 'ic_sanitize_image'
  ]);

  // Control
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_footer_left_image_handle',
    [
      'label'       =>  __('Left Side Image', 'uware'),
      'settings'    =>  'uw_footer_left_image_handle',
      'section'     =>  'uw_footer_section',
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_footer_left_image_link',
    [
      'label'       =>  __('Left Side Image', 'uware'),
      'settings'    =>  'uw_footer_left_image_link',
      'section'     =>  'uw_footer_section',
      'type'        =>  'text'
    ]
  ));
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_footer_copyright_text',
    [
      'label'       =>  __( 'Footer Text', 'uware' ),
      'section'     =>  'uw_footer_section',
      'settings'    =>  'uw_footer_copyright_text',
      'type'        =>  'text',
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_footer_right_image_handle',
    [
      'label'       =>  __('Right Side Image', 'uware'),
      'settings'    =>  'uw_footer_right_image_handle',
      'section'     =>  'uw_footer_section',
    ]
  ));
  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'uw_footer_right_image_link',
    [
      'label'       =>  __('Left Side Image', 'uware'),
      'settings'    =>  'uw_footer_right_image_link',
      'section'     =>  'uw_footer_section',
      'type'        =>  'text'
    ]
  ));



}
