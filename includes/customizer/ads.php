<?php

function uw_ads_customizer_section( $wp_customize ){
  $wp_customize->add_setting( 'uw_ads_handle', [
    'default'         =>   ''
  ]);

  $wp_customize->add_section( 'uw_ads_section',[
    'title'           =>  __( 'Ads Section', 'uware' ),
    'priority'        =>  10,
    'panel'           =>  'uware'
  ]);

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_ads_section',
    [
      'label'       =>  __( 'Ads Code Id', 'uware' ),
      'section'     =>  'uw_ads_section',
      'settings'    =>  'uw_ads_handle',
      'type'        =>  'text',
    ]
  ));
}
