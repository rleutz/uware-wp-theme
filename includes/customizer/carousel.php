<?php

function uw_carousel_customizer_section( $wp_customize ){
  // Section
  $wp_customize->add_section( 'uw_carousel_section',[
    'title'           =>  __( 'Home Carousel Section', 'uware' ),
    'priority'        =>  50,
    'panel'           =>  'uware'
  ]);

  // Settings
  $wp_customize->add_setting('uw_featured_posts_1', [
    'default'    => '',
    'capability' => 'edit_theme_options',
    'type'       => 'option',
  ]);
  $wp_customize->add_setting('uw_featured_posts_2', [
    'default'    => '',
    'capability' => 'edit_theme_options',
    'type'       => 'option',
  ]);
  $wp_customize->add_setting('uw_featured_posts_3', [
    'default'    => '',
    'capability' => 'edit_theme_options',
    'type'       => 'option',
  ]);
  $wp_customize->add_setting('uw_featured_posts_4', [
    'default'    => '',
    'capability' => 'edit_theme_options',
    'type'       => 'option',
  ]);
  $wp_customize->add_setting('uw_featured_posts_5', [
    'default'    => '',
    'capability' => 'edit_theme_options',
    'type'       => 'option',
  ]);


  // Control
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_featured_posts_1_control', [
    'label' => __( 'Left Side Carousel', 'uware' ),
    'description' => __('Left Side Carousel Posts.', 'uware'),
    'section'    => 'uw_carousel_section',
    'settings' => 'uw_featured_posts_1',
    'type' => 'select',
    'choices' => get_categories_select(),
  ]));
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_featured_posts_2_control', [
      'label' => __( 'Center Carousel', 'uware' ),
      'description' => __('Center Carousel Posts.', 'uware'),
      'section'    => 'uw_carousel_section',
      'settings' => 'uw_featured_posts_2',
      'type' => 'select',
      'choices' => get_categories_select(),
  ]));
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_featured_posts_3_control', [
      'label' => __( 'Right Side Carousel', 'uware' ),
      'description' => __('Right Side Carousel Posts.', 'uware'),
      'section'    => 'uw_carousel_section',
      'settings' => 'uw_featured_posts_3',
      'type' => 'select',
      'choices' => get_categories_select(),
    ]
  ));
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_featured_posts_4_control', [
      'label' => __( 'Right Side Down Carousel', 'uware' ),
      'description' => __('Right Side Down Carousel Posts.', 'uware'),
      'section'    => 'uw_carousel_section',
      'settings' => 'uw_featured_posts_4',
      'type' => 'select',
      'choices' => get_categories_select(),
    ]
  ));
  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'uw_featured_posts_5_control', [
      'label' => __( 'Left Side Down Carousel', 'uware' ),
      'description' => __('Left Side Down Carousel Posts.', 'uware'),
      'section'    => 'uw_carousel_section',
      'settings' => 'uw_featured_posts_5',
      'type' => 'select',
      'choices' => get_categories_select(),
    ]
  ));

}
function get_categories_select() {
  $teh_cats = get_categories();
  $results = [];

  $count = count($teh_cats);
  for ($i = 0; $i < $count; $i++) {
    if (isset($teh_cats[$i]))
      $results[$teh_cats[$i]->slug] = $teh_cats[$i]->name;
    else
      $count++;
  }
  return $results;
}
