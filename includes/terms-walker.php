<?php

class Terms_walker extends Walker_Nav_menu {

  public function start_el( &$output, $item, $depth = 0, $args = [], $id =0 ){
    $output   .=  '<li class="list-inline-item">';
    $output   .=  $args->before;
    $output   .=  '<a href="'. $item->url .'" class="badge rounded-pill bg-secondary text-wrap text-light text-decoration-none me-3 mt-2 mb-2">';
    $output   .=  $args->link_before . $item->title . $args->link_after;
    $output   .=  '</a>';
    $output   .=  $args->after;
  }

  public function end_el( &$output, $item, $depth = 0, $args = [], $id =0 ){
    $output   .=  '</li>';
  }
}
