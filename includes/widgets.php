<?php


function uw_widgets() {
    register_sidebar([
      'name'          =>    __( 'Right Sidebar', 'uware'),
      'id'            =>    'uw_sidebar_right',
      'description'   =>    __( 'Right sidebar for theme uware.', 'uware'),
      'before_widget' =>    '<div class="mb-2 mt-3">',
      'after_widget'  =>    '</div>',
      'before_title'  =>    '<h4>',
      'after_title'   =>    '</h4>'
    ]);
}
