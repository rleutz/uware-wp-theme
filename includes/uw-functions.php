<?php

function get_category_id( $catName ){
  $cats = get_categories();
  foreach ($cats as $key) {
    if($key->slug == $catName){
      return $key->term_id;
    }
  }
}
// Minhas funções
//
function site_ads(){
    return "";
}
function sterm_text($atts, $content = null) {
    $dir = "~";
    if(isset($atts)){
      if(isset($atts['dir']) && !empty($atts['dir'])) {
        $dir = $atts['dir'];
      }
    }
    $text = '<style>
    #program_text {
            background-color: black;
            color: white;
            padding: 10px;
            border-radius: 5px;
            font-family: monospace;
    }
    </style>
    <div id="program_text" class="m-2">'."[".get_the_author_meta('user_nicename')."@uware ".$dir."]$ ".trim($content).'</div>';
    return $text;
    return '';
}
function add_asyncdefer_attribute($tag, $handle) {
        // if the unique handle/name of the registered script has 'async' in it
        if (strpos($handle, 'async') !== false) {
            // return the tag with the async attribute
            return str_replace( '<script ', '<script async ', $tag );
        }
        // if the unique handle/name of the registered script has 'defer' in it
        else if (strpos($handle, 'defer') !== false) {
            // return the tag with the defer attribute
            return str_replace( '<script ', '<script defer ', $tag );
        }
        // otherwise skip
        else {
            return $tag;
        }
    }
