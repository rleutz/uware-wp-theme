<?php

function uw_customizer_register( $wp_customize ){
  $wp_customize->add_panel( 'uware', [
    'title'         =>  __( 'Uware Theme', 'uware' ),
    'description'   =>  '<p>Uware Theme Settings</p>',
    'priority'      =>  160
  ]);

  uw_social_customizer( $wp_customize );
  uw_footer_customizer_section( $wp_customize );
  uw_sidebar_customize_section( $wp_customize );
  uw_header_customizer_section( $wp_customize );
  uw_ads_customizer_section( $wp_customize );
  uw_nav_brand_customizer_section( $wp_customize );
  uw_carousel_customizer_section( $wp_customize );
  uw_posts_customizer_section( $wp_customize );
}
