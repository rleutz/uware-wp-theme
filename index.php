<?php get_header(); ?>


<div class="mt-3 mb-3">
  <div class="row">
      <div class="col-12">

          <?php
              if( have_posts() ){
                  while ( have_posts() ) {
                      the_post();

                      get_template_part( 'partials/post/content', 'excerpt' );
                  }
              }
           ?>
      </div>
  </div>
  <div class="row">
    <div class="col-12">
      <?php
        get_template_part( 'partials/all/pagination' );
      ?>
    </div>
  </div>
</div>


<?php get_footer(); ?>
