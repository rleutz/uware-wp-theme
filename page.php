<?php get_header(); ?>


<div class="content mt-3 mb-3">
  <div class="row">
      <div class="col-md-12">

          <?php
              if( have_posts() ){
                  while ( have_posts() ) {
                      the_post();

                      global $post;

                      get_template_part( 'partials/page/content', 'page' );
                  }
              }
           ?>
      </div>
  </div>
</div>


<?php get_footer(); ?>
