<?php get_header(); ?>

<div class="mb-3">
  <div class="row" style="min-height: 300px;">
      <div class="col-xl-4 mt-3">
        <?php
          set_query_var( 'category', get_option('uw_featured_posts_1') );
          get_template_part( 'partials/all/carousel' );
        ?>
      </div>
      <div class="col-xl-4 mt-3">
        <?php
          set_query_var( 'category', get_option('uw_featured_posts_2') );
          get_template_part( 'partials/all/carousel' );
        ?>
      </div>
      <div class="col-xl-4 mt-3">
        <?php
          set_query_var( 'category', get_option('uw_featured_posts_3') );
          get_template_part( 'partials/all/carousel' );
        ?>
      </div>
  </div>
  <div class="row" style="min-height: 300px;">
      <div class="col-xl-4 mt-3">
        <?php
          set_query_var( 'category', get_option('uw_featured_posts_5') );
          get_template_part( 'partials/all/carousel' );
        ?>
      </div>
      <div class="col-xl-4 mt-4">
        <?php
          get_template_part( 'partials/all/social', 'home' );
        ?>
      </div>
      <div class="col-xl-4 mt-3">
        <?php
          set_query_var( 'category', get_option('uw_featured_posts_4') );
          get_template_part( 'partials/all/carousel' );
        ?>
      </div>
  </div>
  <div class="row mt-3">
    <div class="row">
      <div class="col-xl-6">
        <hr class="uw-hr">
      </div>
      <div class="col-xl-6">
        <hr class="uw-hr">
      </div>
    </div>
    <div class="uw-hr-fl">
      <hr>
    </div>
      <?php
          if( have_posts() ){
              $count = 1;
              while ( have_posts() ) {
                ?>
                <div class="col-xl-6">
                <?php
                  the_post();
                  get_template_part( 'partials/post/content', 'card' );
                ?>
                </div>
                <div class="uw-hr-fl">
                  <hr>
                </div>
                <?php
                if($count % 2 == 0){
                  ?>
                  <div class="row">
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                  </div>
                  <?php
                }
                else if (
                  ($wp_query->current_post +1) == ($wp_query->post_count) &&
                  $count % 2 != 0
                  ) {
                    ?>
                    <div class="row">
                      <div class="col-xl-6">
                        <hr class="uw-hr">
                      </div>
                    </div>
                    <?php
                }
                $count = $count+1;
              }
          }
       ?>
    </div>
       <div class="row">
         <div class="col-12">
           <?php
             get_template_part( 'partials/all/pagination' );
           ?>
         </div>
       </div>
  </div>
</div>

<?php get_footer(); ?>
