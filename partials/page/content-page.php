<div class="row mt-3">
  <div class="col-12">
    <h1 class="text-success"><?php the_title(); ?></h1>
    <br>
    <i class="far fa-calendar-alt"></i> <?php echo get_the_date(); ?>
    <p>


        <?php
          if(has_post_thumbnail()){
            the_post_thumbnail('full', [ 'class' => 'rounded img-fluid']);
          }
        ?>


      <?php
      the_content();
      ?>
    </p>
  </div>
</div>
