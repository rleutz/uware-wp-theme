
<div class="row mt-3">
  <div class="col-12">
    <div class="card bg-light">
      <h2 class="card-header text-white bg-success text-center">
        <i class="fas fa-file-alt"></i>&ensp;<?php the_title(); ?>
      </h2>
      <div class="card-body">
        <i class="far fa-calendar-alt"></i> <?php echo get_the_date(); ?>
        <br>
        <div class="row">
          <div class="col-sm-6">
            <a class="text-decoration-none text-success">
              <i class="fas fa-list-alt"></i>&ensp;<?php the_category( ' ' ); ?>
            </a>
          </div>
          <div class="col-sm-6">
            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="text-decoration-none text-success">
              <div class="row row-cols-sm-auto float-end">
                <div class="col">
                    <?php echo get_the_author_meta('first_name') . " " . get_the_author_meta('last_name'); ?>
                </div>
                <div class="col">
                  <?php echo get_avatar( get_the_author_meta( 'ID' ), 30, '', false, [ 'class' => 'rounded-circle img-fluid author-photo' ] ); ?>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
        <?php
          if(has_post_thumbnail()){
            the_post_thumbnail('large', [ 'class' => 'card-img-bottom']);
          }
        ?>
    </div>
        <hr>
      <p>
      <?php
      the_content();
      $args = array (
          'before'            => '<div class="text-center mb-3">',
          'after'             => '</div>',
          'link_before'       => '<span class="btn btn-secondary">',
          'link_after'        => '</span>',
          'next_or_number'    => 'next',
          'separator'         => ' ',
          'nextpagelink'      => __( 'Próximo', 'uware' ),
          'previouspagelink'  => __( 'Anterior', 'uware' ),
      );

      wp_link_pages( $args );
      ?>

      <?php
      if( has_tag() ) {
        ?>
        <hr>
        <?php
        the_tags( '<p class="lead">Tags: </p> <i class="fas fa-paperclip text-secondary"></i>', '&ensp;<i class="fas fa-paperclip text-secondary"></i>' );
      }
      ?>
      <hr>
      <?php
      get_template_part('partials/all/author');
      ?>
      <hr>
      <div class="row">
        <div class="col">
          <?php
            previous_post_link();
          ?>
        </div>
        <div class="col">
          <div class="float-end">

          <?php
            next_post_link();

          ?>
          </div>
        </div>
      </div>
      <hr>
      <div class="clearfix">
      <p class="lead">
        Posts relacionados:
      </p>
      <?php
      $categories           =  get_the_category( );
      $rp_query = new Wp_Query([
        'posts_per_page'    =>  5,
        'post__not_in'      =>  [ $post->ID ],
        'cat'               =>  !empty($categories) ? $categories[0]->term_id : null
      ]);


      if( $rp_query->have_posts() ){
        while( $rp_query->have_posts() ) {
          $rp_query->the_post();
          get_template_part( 'partials/post/content', 'excerpt' );
        }

        wp_reset_postdata();
      }

      ?>
      </div>

    </p>
  </div>
</div>
