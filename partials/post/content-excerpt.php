
<div class="row mt-3">
  <div class="col-12">
      <div class="card">
        <div class="card-header bg-success lead fw-bold">
            <a href="<?php the_permalink(); ?>" class="text-decoration-none text-white">
              <i class="fas fa-file-alt"></i>&ensp;<?php the_title(); ?>
            </a>
        </div>
        <div class="card-body bg-light">

          <p class="card-title">
            <div class="row">
              <div class="col-sm-6">
                <a class="text-decoration-none text-success">
                  <i class="fas fa-list-alt"></i>&ensp;<?php the_category( ' ' ); ?>
                </a>
              </div>
              <div class="col-sm-6">
                <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="text-decoration-none text-success">
                  <div class="row row-cols-sm-auto float-end">
                    <div class="col">
                        <?php echo get_the_author_meta('first_name') . " " . get_the_author_meta('last_name'); ?>
                    </div>
                    <div class="col">
                      <?php echo get_avatar( get_the_author_meta( 'ID' ), 30, '', false, [ 'class' => 'rounded-circle img-fluid author-photo' ] ); ?>
                    </div>

                  </div>
                </a>
              </div>
            </div>

          </p>
          <p>

              <div class="row">
                <?php
                if(has_post_thumbnail()){
                 ?>
                <div class="col-md-3">
                <a href="<?php the_permalink(); ?>" class="text-decoration-none text-success">
                <?php
                  the_post_thumbnail('thumbnail', [ 'class' => 'img-fluid rounded']);
                ?>
                </a>
                </div>
                <div class="col-md-9">
                <?php
              } else {
                ?>
                <div class="col-lg-12">
                <?php
              }
              ?>
                    <a href="<?php the_permalink(); ?>" class="text-decoration-none text-muted"><?php the_excerpt(); ?></a>
                </div>

                </div>
          </p>
        </div>

        <div class="card-footer text-muted">
          <i class="far fa-calendar-alt"></i>
          <a href="<?php the_permalink(); ?>" class="text-decoration-none text-muted">
            <?php echo get_the_date(); ?>
          </a>
        </div>
      </div>
  </div>
</div>
</a>
