<a href="<?php the_permalink(); ?>">
<div class="card mb-3 border-0">
  <div class="row g-0">
      <div class="col-md-4">
        <?php
        if(has_post_thumbnail()){
          the_post_thumbnail('thumbnail', [ 'class' => 'img-fluid rounded']);
        }
        ?>
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_excerpt(); ?></p>
          <p class="card-text">
            <small class="text-muted">
              <i class="far fa-calendar-alt"></i>&ensp;<?php echo get_the_date(). '&ensp; - &ensp;<i class="fas fa-list-alt"></i>&ensp;'.get_the_category( $id )[0]->name; ?>
            </small>
          </p>
        </div>
      </div>
  </div>
</div>
</a>
