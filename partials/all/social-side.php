<style>
.icon-bar {
  max-width: 50px;
  position: fixed;
  top: 30%;
  left:0;
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 10px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: gray;
}

.facebook {
  background: #3B5998;
  color: white;
  border-radius: 0 5px 0 0;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.telegram {
  background: #0088cc;
  color: white;
  border-radius: 0 0 5px 0;
}

.youtube {
  background: #bb0000;
  color: white;
}
@media only screen and (max-width: 1450px) {
  .icon-bar {
    display: none;
  }
}

</style>
<div class="icon-bar shadow-uware">
  <a href="https://www.facebook.com/<?php echo get_theme_mod( 'uw_facebook_handle' ); ?>" class="facebook" target="_blank"><i class="fab fa-facebook"></i></a>
  <a href="https://twitter.com/<?php echo get_theme_mod( 'uw_twitter_handle' ); ?>" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
  <a href="https://www.youtube.com/channel/<?php echo get_theme_mod( 'uw_youtube_handle' ); ?>" class="youtube" target="_blank"><i class="fa fa-youtube"></i></a>
  <a href="https://t.me/<?php echo get_theme_mod( 'uw_telegram_handle' ); ?>" class="telegram" target="_blank"><i class="fab fa-telegram-plane"></i></a>
</div>
