<div class="card text-dark bg-light mb-3">
  <div class="card-header text-white bg-success lead fw-bold">
    <i class="far fa-calendar-alt"></i>&ensp;<?php the_archive_title(); ?>
  </div>
  <div class="card-body">
    <p class="card-text text-muted"><?php the_archive_description(); ?></p>
  </div>
</div>
