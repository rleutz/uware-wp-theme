<div class="card bg-light">
  <div class="card-header bg-success lead fw-bold">
    <i class="fas fa-id-card text-white me-2"></i>
    <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="text-decoration-none text-white">
      <?php echo get_the_author_meta('first_name') . " " . get_the_author_meta('last_name'); ?>
    </a>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-2">
        <?php echo get_avatar( get_the_author_meta( 'ID' ), 120, '', false, [ 'class' => 'rounded-circle img-fluid' ] ); ?>
      </div>

      <div class="col-10">
        <?php echo nl2br( get_the_author_meta( 'description' ) ); ?>
      </div>
    </div>
  </div>
</div>
