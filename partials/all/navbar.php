<style>
@media only screen and (max-width: 1400px) {
  .logo-sm:hover {
    padding-left:<?php echo get_theme_mod('uw_nav_brand_left'); ?>;
    transition: all 0.7s;
  }
  #top-menu {
    min-width: 100%;
  }
}
<?php
if( is_user_logged_in() ) {
    ?>
    .sticky {
      padding-top: 30px;
    }
    <?php
}
?>
</style>

  <div class="row menu mt-2 mb-1" id="top-menu">
    <nav class="navbar navbar-expand-xxl navbar-light bg-light rounded shadow-uware">

        <a class="navbar-brand ms-0 shadow-uware-sm" href="/">
          <img id="nav-brand-logo" class="logo-sm" src="<?php echo get_theme_mod( 'uw_nav_brand_handle' ); ?>">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="inline collapse navbar-collapse" id="navbarSupportedContent">

        <div class="col-xxl-9">
        <?php
        if( has_nav_menu('primary')){
          wp_nav_menu([
            'menu'    => 'main-menu',
            'theme_location'  => 'primary',
            'menu_class'      => 'navbar-nav mx-auto mb-2 mb-lg-0 position-relative',
            'depth'    => 5,
            'fallback_cb'     => '__return_false',
            'walker'          => new Bootstrap5_Nav_Walker()
          ]);
        }
        ?>
        </div>
        <div class="col-xxl-3">
        <?php
          get_search_form( [ 'class' => 'float-end' ] );
        ?>
        </div>
      </div>



  </nav>
</div>
<div id="uware-up-arrow" class="uware-arrow p-2">
    <h1><a onclick="topFunction()"><i class="fas fa-arrow-circle-up"></i></a></h1>
</div>
