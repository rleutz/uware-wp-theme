<div class="card text-center">
  <div class="card-header">
    <?php
    $cat = get_category(get_category_id( get_query_var( 'category', $default = 1 ) ) );
    ?>
    <a href="<?php echo '/category/'.$cat->slug; ?>" class="">
        <i class="fas fa-list-alt"></i>&ensp;<?php echo $cat->name; ?>
    </a>
  </div>
  <div class="card-body  p-0">
    <div id="<?php echo $cat->slug; ?>" class="carousel slide rounded-bottom" data-bs-ride="carousel">
      <div class="carousel-inner">
        <?php
        $args = array(
          'posts_per_page' => 5,
          'orderby' => 'date',
          'order' => 'DESC',
          "category__in" => [ $cat->term_id ]
        );
        $query = new WP_Query( $args );
        $count = 0;
        // var_dump( $query );
        $count = 0;
        while( $query->have_posts() ){
          set_query_var( 'count', $count );
          $query->the_post();
        ?>
        <div class="carousel-item
        <?php
        if($count == 0) {
          echo ' active';
          $count++;
        }
        ?>">
        <a href="<?php the_permalink(); ?>">
        <?php
          the_post_thumbnail( 'medium', [
            'class'   =>  'd-block w-100 carousel-img card-img-bottom'
            ] );
        ?>
        </a>
          <div class="carousel-caption d-none d-md-block carousel-uware">
            <a href="<?php the_permalink(); ?>">
              <h5><?php the_title(); ?></h5>
            </a>
          </div>
        </div>
        <?php
        $count++;
      }
      wp_reset_query();
      ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#<?php echo $cat->slug; ?>" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#<?php echo $cat->slug; ?>" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
    </div>
  </div>
</div>
