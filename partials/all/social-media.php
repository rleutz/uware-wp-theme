<div id="social-media" class="row bg-secondary border border-secondary border-top-0 p-1
    <?php
    if( !get_theme_mod('uw_header_present') ) {
      echo ' rounded-bottom';
    }
    ?>">
    <div class="col-6">
        <i class="fas fa-network-wired text-light"></i>
        <?php
        if( get_theme_mod('uw_social_title_handle') ){
        ?>
        <span class="text-light"><?php echo get_theme_mod( 'uw_social_title_handle' ); ?></span>
        <?php
        }
        ?>
    </div>
    <div class="col-6 text-end">
        <?php
        if( get_theme_mod( 'uw_telegram_handle') ){
        ?>
        <a href="https://t.me/<?php echo get_theme_mod( 'uw_telegram_handle' ); ?>" class="telegram-icon rounded p-2 text-light" target="_blank">
          <i class="fab fa-telegram-plane" alt="Telegram"><span
                class="telegram-text text-center">&nbsp;Telegram</span></i></a>
        <?php
        }
        if( get_theme_mod( 'uw_youtube_handle' ) ){
        ?>
        <a href="https://www.youtube.com/channel/<?php echo get_theme_mod( 'uw_youtube_handle' ); ?>" class="youtube-icon rounded p-2 text-light" target="_blank">
          <i class="fab fa-youtube" alt="Youtube"><span
                class="youtube-text text-center">&nbsp;YouTube</span></i></a>
        <?php
        }
        if( get_theme_mod( 'uw_twitter_handle' ) ){
        ?>
        <a href="https://twitter.com/<?php echo get_theme_mod( 'uw_twitter_handle' ); ?>" class="twitter-icon rounded p-2 text-light" target="_blank">
          <i class="fab fa-twitter" alt="Twitter"><span
                class="twitter-text text-center">&nbsp;Twitter</span></i></a>
        <?php
        }
        if( get_theme_mod( 'uw_facebook_handle' ) ){
         ?>
        <a href="https://www.facebook.com/<?php echo get_theme_mod( 'uw_facebook_handle' ); ?>" class="facebook-icon rounded p-2 text-light" target="_blank">
          <i class="fab fa-facebook" alt="Facebook"><span
                class="facebook-text text-center">&nbsp;Facebook</span></i></a>
        <?php
        }
        ?>
    </div>
</div>
