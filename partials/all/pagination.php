<div class="row">
  <div class="col-12 mt-5">
    <ul class="pagination justify-content-center">
      <?php
      if ( null === $wp_query ) {
          global $wp_query;
      }
      $total = $wp_query->max_num_pages;
      if ( $total > 1 )  {

           if ( !$current_page = get_query_var('paged') ){
                $current_page = 1;
              }

           if( empty(get_option('permalink_structure')) ) {
              $format = '&page=%#%';
              } else {
              $format = 'page/%#%/';
              }
           $page = paginate_links( [
                'base'        => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'format'      => $format,
                'current'     => $current_page,
                'total'       => $total,
                'mid_size'    => 2,
                'type'        => 'array',
                'next_text'   => '>',
                'prev_text'   => '<',
                'before_page_number' => '',
                'after_page_number' => ''
           ] );

          foreach($page as $item){
              $new = str_replace( '<span aria-current="page" class="', '<li class="page-item disabled"><span class="page-link ', $item);
              $new = str_replace( '<span class="page-numbers dots">&hellip;</span>', '<li class="page-item disabled"><spam class="page numbers page-link">...</span></li>', $new);
              $new = str_replace( '<a class="', '<li class="page-item"><a class="page-link text-success text-bold ', $new);
              $new = str_replace( '</a>', '</a></li>', $new);
              $new = str_replace( '</span>', '</span></li>', $new );
              echo $new;
          }
      }
      wp_reset_query();
      ?>
    </ul>
  </div>
</div>
