<?php
if( has_nav_menu( 'terms' ) ){
  wp_nav_menu( [
    'theme_location'    => 'terms',
    'container'         => false,
    'fallback_cb'       => false,
    'depth'             => 1,
    'walker'            => new Terms_walker(),
    'menu_class'        => 'list-inline mb-0 pb-0',
    'container'         => 'div',
    'container_class'   => 'row vertical-center shadow-uware-sm'
  ]);
}
