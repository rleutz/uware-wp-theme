<div class="card text-center">
  <div class="card-header">
    <i class="fas fa-network-wired"></i>&nbsp;<?php echo get_theme_mod( 'uw_social_title_handle' ); ?>
  </div>
  <div class="card-body bg-light">
    <br>
    <?php
    if( get_theme_mod( 'uw_telegram_handle') ){
    ?>
    <a href="https://t.me/<?php echo get_theme_mod( 'uw_telegram_handle' ); ?>" class="telegram-color" target="_blank">
      <h5 class="card-title"><i class="fab fa-telegram-plane" alt="Telegram"><span
            class="">&nbsp;Telegram</span></i></h5>
    </a>
    <?php
    }
    if( get_theme_mod( 'uw_youtube_handle' ) ){
    ?>
    <br>
    <a href="https://www.youtube.com/channel/<?php echo get_theme_mod( 'uw_youtube_handle' ); ?>" class="youtube-color" target="_blank">
      <h5 class="card-title">
        <i class="fab fa-youtube" alt="Youtube"><span
              class="">&nbsp;YouTube</span></i>
      </h5>
    </a>
    <br>
    <?php
    }
    if( get_theme_mod( 'uw_twitter_handle' ) ){
    ?>
    <a href="https://twitter.com/<?php echo get_theme_mod( 'uw_twitter_handle' ); ?>" class="twitter-color" target="_blank">
      <h5 class="card-title">
        <i class="fab fa-twitter" alt="Twitter"><span
              class="">&nbsp;Twitter</span></i>
      </h5>
    </a>
    <br>
    <?php
    }
    if( get_theme_mod( 'uw_facebook_handle' ) ){
     ?>
    <a href="https://www.facebook.com/<?php echo get_theme_mod( 'uw_facebook_handle' ); ?>" class="facebook-color" target="_blank">
      <h5 class="card-title">
        <i class="fab fa-facebook" alt="Facebook"><span
              class="">&nbsp;Facebook</span></i>
      </h5>
    </a>
    <?php
    }
    ?>
    <br>
  </div>
</div>
