<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">
    <head>
        <meta http-equiv="content-type" context="text/html; charset=utf-8" />

        <?php wp_head(); ?>

        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php
        if( get_theme_mod( 'uw_ads_handle' ) ){
        ?>
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=<?php echo get_theme_mod( 'uw_ads_handle' ); ?>"
            crossorigin="anonymous"></script>
        <?php
        }
        ?>

    </head>
    <body <?php body_class('container-xxl content'); ?>>

              <div id="top" class="shadow-uware mb-0 pb-0">
                  <div class="row border border-top-0 border-bottom-0 border-light bg-light mb-0 pb-0 vertical-center">
                    <?php
                      get_template_part( 'partials/all/terms-menu' );
                    ?>
                  </div>


                  <?php
                    get_template_part( 'partials/all/social-media' );
                  ?>

                  <?php
                  if( get_theme_mod( 'uw_header_present') ) {
                   ?>
                  <div class="row bg-light rounded-bottom mb-2">
                      <div class="col-lg-2 pt-3 pb-3 shadow-uware logo text-center">
                          <?php
                          if( has_custom_logo() ){
                            the_custom_logo();

                          } else {
                            ?>
                            <a href="<?php echo home_url( '/' ); ?>" class="shadow-uware ms-3" src="../images/logo_redondo.webp"
                                alt="Logo" style="max-height: 150px;"/><?php echo bloginfo( 'name' ); ?></a>
                            <?php
                          }

                           ?>
                      </div>
                      <div class="col-lg-8 text-end mt-2">

                      </div>
                  </div>
                  <?php
                  }
                  ?>
              </div>

              <?php
                get_template_part( 'partials/all/navbar' );
                if( get_theme_mod( 'uw_social_side_present' ) ){
                  get_template_part( 'partials/all/social-side' );
                }
              ?>
