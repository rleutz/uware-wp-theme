<?php get_header(); ?>
<div class="mt-5 mb-3">
  <div class="row mb-3">
      <div class="col-2"></div>
      <div class="col-8">
        <div class="card shadow-uware">
          <div class="card-header">
            <?php _e('Resultados da procura por: ', 'uware'); ?>
          </div>
          <div class="card-body">
            <blockquote class="blockquote mb-0">
              <footer class="blockquote-footer mt-1"><?php echo esc_html( get_search_query( false ) ); ?></footer>
              <?php get_search_form(); ?>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="col-2"></div>
  </div>
  <div class="row">
      <div class="col-12">
          <?php

            if( have_posts() ){
                  while ( have_posts() ) {
                      the_post();

                      get_template_part( 'partials/post/content', 'excerpt' );
                  }
            }
           ?>
      </div>
      <div class="row">
        <div class="col-12">
          <?php
            get_template_part( 'partials/all/pagination' );
          ?>
        </div>
      </div>
  </div>
</div>
<?php get_footer(); ?>
