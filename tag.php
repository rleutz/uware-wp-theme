<?php
get_header();
?>
<div class="mt-5 mb-3">
  <div class="row">

      <div class="row justify-content-center mb-5">
        <div class="col-xl-9">
          <?php
            get_template_part( 'partials/all/tag' );
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-6">
          <hr class="uw-hr">
        </div>
        <div class="col-xl-6">
          <hr class="uw-hr">
        </div>
      </div>
      <div class="uw-hr-fl">
        <hr>
      </div>
      <?php
          if( have_posts() ){
              $count = 1;
              while ( have_posts() ) {
                ?>
                <div class="col-xl-6">
                <?php
                  the_post();
                  get_template_part( 'partials/post/content', 'card' );
                ?>
                </div>
                <div class="uw-hr-fl">
                  <hr>
                </div>
                <?php
                if($count % 2 == 0){
                  ?>
                  <div class="row">
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                  </div>
                  <?php
                }
                else if (
                  ($wp_query->current_post +1) == ($wp_query->post_count) &&
                  $count % 2 != 0
                  ) {
                    ?>
                    <div class="row">
                      <div class="col-xl-6">
                        <hr class="uw-hr">
                      </div>
                    </div>
                    <?php
                }
                $count = $count+1;
              }
          }
       ?>
      </div>
      <div class="row">
        <div class="col-12">
          <?php
            get_template_part( 'partials/all/pagination' );
          ?>
        </div>
      </div>
  </div>
</div>


<?php get_footer(); ?>
