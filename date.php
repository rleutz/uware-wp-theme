<?php get_header(); ?>


<div class="mt-3 mb-3">
  <div class="row">

      <div class="mt-3 mb-3 pb-3 pt-3 ps-5 pe-5 bg-light rounded shadow-uware">
        <h2 class="text-muted"><?php the_archive_title( $before = '', $after = '' ) ?></h2>
        <h5 class="lead">
          <?php
            if( is_year() ){
              echo "Você está vendo arquivos do ano.";
            } else if( is_mounth() ) {
              echo "Você está vendo arquivos do mês.";
            } else {
              echo "Você está vendo arquivos do dia.";
            }
          ?>
        </h5>
      </div>

      <div class="col-12">

          <?php
              if( have_posts() ){
                  while ( have_posts() ) {
                      the_post();

                      get_template_part( 'partials/post/content', 'excerpt' );
                  }
              }
           ?>
      </div>
      <div class="row">
        <div class="col-12">
          <?php
            get_template_part( 'partials/all/pagination' );
          ?>
        </div>
      </div>
  </div>
</div>


<?php get_footer(); ?>
