<?php

// Setup
define( 'UW_DEV_MODE', true );

// Includes
include(get_theme_file_path( '/includes/front/enqueue.php' ));
include(get_theme_file_path( '/includes/setup.php' ));
include(get_theme_file_path( '/includes/uw-functions.php' ));
include(get_theme_file_path( '/includes/theme-customizer.php' ));
include(get_theme_file_path( '/includes/customizer/social.php' ));
include(get_theme_file_path( '/includes/customizer/ads.php' ));
include(get_theme_file_path( '/includes/customizer/nav-brand.php' ));
include(get_theme_file_path( '/includes/customizer/carousel.php' ));
include(get_theme_file_path( '/includes/customizer/footer.php' ));
include(get_theme_file_path( '/includes/customizer/header-logo.php' ));
include(get_theme_file_path( '/includes/customizer/sidebar.php' ));
include(get_theme_file_path( '/includes/customizer/posts.php' ));
include(get_theme_file_path( '/includes/bootstrap5-nav-walker.php' ));
include(get_theme_file_path( '/includes/terms-walker.php' ));
include(get_theme_file_path( '/includes/widgets.php' ));

// Hooks
add_action('wp_enqueue_scripts', 'uw_enqueue');
add_action( 'after_setup_theme', 'uw_setup_theme' );
add_action( 'widgets_init', 'uw_widgets' );
add_action( 'customize_register', 'uw_customizer_register' );

// // Shortcodes
add_shortcode('ads','site_ads');
add_shortcode('asb','site_ads');
add_shortcode( 'sterm', 'sterm_text' );


// filters
add_filter('script_loader_tag', 'add_asyncdefer_attribute', 10, 2);
