<?php $unique_id = esc_attr( uniqid('search-form-')); ?>

<form role="search" class="search-form d-flex" method="get"
      action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <input class="form-control me-2"
        type="search"
        name="s"
        id="<?php echo $unique_id; ?>"
        placeholder="<?php _e( 'Procurar', 'uware' ); ?>"
        value="<?php the_search_query(); ?>"
        aria-label="Search">
    <button class="btn btn-outline-success" type="submit">
      <i class="fas fa-search"></i>
    </button>
</form>
