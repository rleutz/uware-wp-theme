<?php
get_header();
$cat = get_category(get_category_id( "kotlinbasico" ) );
$args = array(
  'posts_per_page' => 10,
  'orderby' => 'date',
  'order' => 'ASC',
  "category__in" => [ $cat->term_id ]
);
$query = new WP_Query( $args );
?>
<div class="mt-5 mb-3">
  <div class="row">

    <div class="row justify-content-center mb-5">
      <div class="col-xl-9">
        <?php
          get_template_part( 'partials/all/category' );
        ?>
      </div>
    </div>
      <div class="row">
        <div class="col-xl-6">
          <hr class="uw-hr">
        </div>
        <div class="col-xl-6">
          <hr class="uw-hr">
        </div>
      </div>
      <div class="uw-hr-fl">
        <hr>
      </div>
      <?php
          if( $query->have_posts() ){
              $count = 1;
              while ( $query->have_posts() ) {
                ?>
                <div class="col-xl-6">
                <?php
                  $query->the_post();
                  get_template_part( 'partials/post/content', 'card' );
                ?>
                </div>
                <div class="uw-hr-fl">
                  <hr>
                </div>
                <?php
                if($count % 2 == 0){
                  ?>
                  <div class="row">
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                    <div class="col-xl-6">
                      <hr class="uw-hr">
                    </div>
                  </div>
                  <?php
                }
                else if (
                  ($query->current_post +1) == ($query->post_count) &&
                  $count % 2 != 0
                  ) {
                    ?>
                    <div class="row">
                      <div class="col-xl-6">
                        <hr class="uw-hr">
                      </div>
                    </div>
                    <?php
                }
                $count = $count+1;
              }
              wp_reset_query();
          }
       ?>
      </div>
      <div class="row">
        <div class="col-12">
          <?php
            get_template_part( 'partials/all/pagination' );
          ?>
        </div>
      </div>
  </div>
</div>


<?php get_footer(); ?>
