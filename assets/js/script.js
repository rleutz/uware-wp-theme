window.onscroll = function() {myFunction()};

var topBar = document.getElementById("top");
var navbar = document.getElementById("top-menu");
var upArrow = document.getElementById("uware-up-arrow");
var sticky = navbar.offsetTop;
var navBrandLogo = document.getElementById("nav-brand-logo");
var socialMedia = document.getElementById("social-media");

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        navbar.classList.add("container");
        topBar.classList.add("margin-menu-sticky");
        socialMedia.classList.add("nav-margin-bottom");
        navBrandLogo.classList.remove("logo-sm");
        navBrandLogo.classList.add("logo-sm-footer");
        if(getWidth() > 1400){
          upArrow.style.display = 'block';
        }
    } else {
        navbar.classList.remove("container");
        navbar.classList.remove("sticky");
        topBar.classList.remove("margin-menu-sticky");
        socialMedia.classList.remove("nav-margin-bottom");
        navBrandLogo.classList.add("logo-sm");
        navBrandLogo.classList.remove("logo-sm-footer");
        upArrow.style.display = 'none';
    }
}

function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}
