<?php get_header();
$cols = get_theme_mod( 'uw_post_cols_number' );
$cols_side = (12-$cols)/2;
?>
<div class="content mt-3 mb-3">
  <div class="row <?php
    if( $cols % 2 != 0 && !get_theme_mod( 'uw_sidebar_right_toogle' ) ){
      echo 'justify-content-center';
    }
  ?>">
    <?php
    if( !get_theme_mod('uw_sidebar_full_width_toogle') ){
      if( $cols % 2 == 0 && !get_theme_mod( 'uw_sidebar_right_toogle' ) ){
      ?>
      <div class="col-xl-<?php echo $cols_side; ?>"></div>
      <div class="col-xl-<?php echo $cols; ?>">
    <?php
      } else {
        ?>
        <div class="col-xl-<?php echo $cols; ?>">
        <?php
      }
    } else {
      ?>
      <div class="col-xl-12">
      <?php
    }
    ?>
          <?php
              if( have_posts() ){
                  while ( have_posts() ) {
                      the_post();
                      global $post;
                      get_template_part( 'partials/post/content', 'post' );
                  }
              }
           ?>
      </div>
      <?php
        if( get_theme_mod( "uw_sidebar_right_toogle" ) &&
            !get_theme_mod( 'uw_sidebar_full_width_toogle') ){
      ?>
          <div class="col-xl-3">
            <?php
              get_sidebar('uw_sidebar_right');
            ?>
          </div>
      <?php
        }
      ?>
  </div>
  <?php
  if( !get_theme_mod('uw_sidebar_full_width_toogle') ){
    if( $cols % 2 == 0 && !get_theme_mod( 'uw_sidebar_right_toogle' ) ){
  ?>
    <div class="col-xl-<?php echo $cols_side; ?>"></div>
  <?php
    }
  }
  ?>
</div>


<?php get_footer(); ?>
