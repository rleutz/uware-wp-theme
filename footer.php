<div id="footer">
<footer class="footer mb-0 shadow-uware">
    <div class="row border border-bottom-0 border-light p-2 bg-light rounded-top">
        <div class="col-2">
          <?php
          if( get_theme_mod( 'uw_footer_left_image_handle' ) ) {
          ?>
          <a href="<?php echo get_theme_mod( 'uw_footer_left_image_link' ); ?>" target="_blank">
            <img class="logo-sm-footer shadow-uware-sm" src="<?php echo get_theme_mod( 'uw_footer_left_image_handle' ); ?>">
          </a>
          <?php
          }
          ?>
        </div>
        <div class="col text-center">
            <span><?php echo get_theme_mod( 'uw_footer_copyright_text' ); ?></span>
        </div>
        <div class="col-2">
          <?php
          if( get_theme_mod( 'uw_footer_right_image_handle' ) ) {
          ?>
          <a href="<?php echo get_theme_mod( 'uw_footer_right_image_link' ); ?>" target="_blank">
            <img class="logo-sm-footer float-end shadow-uware-sm" src="<?php echo get_theme_mod( 'uw_footer_right_image_handle' ); ?>">
          </a>
          <?php
          }
          ?>
        </div>
    </div>
</footer>
</div>

        <?php wp_footer(); ?>
    </body>

</html>
